define(function(require,exports,module) {
	function kai () {
		console.log('start ... ');
	}
	
	var KaiJSON = require('famous/jquery-1.7.2.min');//'http://code.jquery.com/jquery-1.7.2.min.js'
	var json;
	var objectlist = new Object();

	var devicetype = 'android';
		
	kai.prototype.readJSON2 = function (url) {
		KaiJSON.getJSON(url, function(data) {//data is the JSON string
			console.log('url ' + data);
        });
	}
	
	//this is used to fetch json-file from contentmanager
	kai.prototype.readJSON = function () {
		/*
		$.getJSON('Access-Control-Allow-Origin: http://droid.duckdns.org:8080/database.json', function(data) {//data is the JSON string
			console.log(data);
        });
		*/
		$.ajax({
		  type: 'GET',
		  //crossDomain: true,
		  dataType: 'json',
		  url: './getcontent.json',
		}).done(function ( data ) {
		   //console.log(data);
		   json = data; //
		   createContentList(json);
		   console.log("json: "+json);
		});

		this.checkDevice();

	}
	kai.prototype.getJson = function() {
		return json;
	}
	// used when item selected, return the relative url
	kai.prototype.getUrl4garment = function(evt){
		var fb = evt.data.split(" ")[1]; //'item 5' / 'item 17'
		var newurl = objectlist[fb]; 
		console.log(fb);
		console.log(newurl);
		return newurl;
	}

    //contentid & interactiontype => mapping
	function createContentList (json){
		var content = json.content[0];
		objectlist = new Object();
		for(var i=0; i<content.length; i++){
			objectlist[ content[i].contentid ] = content[i].interactiontype;
		}
	}

	//for x & y pointer shifting
	kai.prototype.caliFunc = function(e) {
		counter += 1;
		var rota = e.rotationRate;
		var shijian = e.interval;
		cali_x = rota.gamma*shijian*para*1 + cali_x ;
		cali_y = rota.alpha*shijian*para*1 + cali_y ;
		shift_x = cali_x / (counter*shijian);
		shift_y = cali_y / (counter*shijian);
	}

	kai.prototype.updateMotion = function updateMotion(e) {
		var rota = e.rotationRate;
		var shijian = e.interval;
		axis[2] = rota.gamma*shijian*para*1 ;//shijian*shift_x为device的偏移差
		axis[0] = rota.alpha*shijian*para*1 ;
		var str = updateRollGesture( axis[2] , axis[0], 0);
		return str;
	}
	
	var updateRollGesture = function( tiltX,tiltY,tiltZ ) {
		tiltBufferX.update(tiltX);
		tiltBufferY.update(tiltY);
		tiltBufferZ.update(tiltZ);
		prev_x = x;
		prev_y = y;
		prev_z = z;
		x = tiltBufferX.sum();
		y = tiltBufferY.sum();
		z = tiltBufferZ.sum();

		return "xy "+ x + " " + y;
	}

	kai.prototype.calibrate = function() {//this is recalibration part, center the pointer
		axis[2] = CENTER_X;
		axis[0] = CENTER_Y;
		tiltBufferX[0] = CENTER_X;//!!!
		tiltBufferX[1] = CENTER_X;
		tiltBufferY[0] = CENTER_Y;
		tiltBufferY[1] = CENTER_Y;
	}

	//http://stackoverflow.com/questions/18112729/calculate-compass-heading-from-deviceorientation-event-api
	kai.prototype.calibrateTo = function(e) {
		var dir_raw = 360 - e.alpha;
		if(devicetype=='android') {
			dir_raw = 360 - e.alpha;
			if(dir_raw > 115.0) {
				CENTER_X = -2.5;//you
			} else {
				CENTER_X = 2.5;//zuo
			}
		}else if(devicetype=='ios'){ //ios
			dir_raw = e.webkitCompassHeading;
			if(dir_raw > 119.0) {//you
				CENTER_X = -2.5;
			} else {
				CENTER_X = 2.5;
			}
		}
		
		axis[2] = CENTER_X;
		axis[0] = CENTER_Y;
		tiltBufferX[0] = CENTER_X;//!!!
		tiltBufferX[1] = CENTER_X;
		tiltBufferY[0] = CENTER_Y;
		tiltBufferY[1] = CENTER_Y;
		console.log('center x: '+ CENTER_X);
	}
    

    var cali_x = 0;
    var cali_y = 0;
    var counter = 0;
	var shift_x = 0.000;//x calibration shifting
	var shift_y = 0.000;//y calibration shifting

	var CENTER_X = -1;
	var CENTER_Y = 0;
	
	var axis = [CENTER_Y,0,CENTER_X];//raw data for pointer x&y moving
	var bufferSize = 2;
	var tiltCorrection = 2.5;
	var tiltBufferX = new ValueBuffer(bufferSize);
	var tiltBufferY = new ValueBuffer(bufferSize);
	var tiltBufferZ = new ValueBuffer(bufferSize);
	var tiltZAxis = 0;
	var tiltXAxis = 0;
	var tiltYAxis = 0;
	var tiltXAxis_refer = 0;
	var tiltZAxis_prev = 0;
	var tiltXAxis_prev = 0;
	var tiltYAxis_prev = 0;
	var para= 2.0/100.0;
	var prev_x = 0;
	var prev_y = 0;
	var prev_z = 0;
	var x = 0;
	var y = 0;
	var z = 0;
	
	kai.prototype.checkDevice = function() {
		var deviceAgent = navigator.userAgent.toLowerCase();
        var isTouchDevice = (deviceAgent.match(/(android)/)  || deviceAgent.match(/bada/i));
        if (isTouchDevice) {
        	devicetype = 'android';
            console.log('device:a');
        } else {
        }
        var isIITouchDevice = (deviceAgent.match(/(iphone|ipod|ipad)/) );
        if (isIITouchDevice) {
        	devicetype = 'ios';
            para = 20 / 100.0;
            console.log('device:i');
        } else {
		
        }
    }

	//for ValueBuffer
	ValueBuffer.prototype.initBuffer = function() {
		this.sampleIndex = 0;
		this.buffer = [];
		for (var i=0; i < this.numSamples; i++) this.buffer.push(0);
	};
	ValueBuffer.prototype.update = function( value ) {
		this.sampleIndex++;
		if (this.sampleIndex == this.numSamples) this.sampleIndex = 0;
		this.buffer[this.sampleIndex] = value;
	};
	ValueBuffer.prototype.sum = function() {
		var sum = 0;
		for (var i=0; i < this.numSamples; i++) {
			sum += this.buffer[i];
		}
		return sum;
	};
	function ValueBuffer( numSamples ) {
		this.numSamples = numSamples; //this.initBuffer();
		this.sampleIndex = 0;
		this.buffer = [];
		for( var i=0; i < this.numSamples; i++) this.buffer.push(0);
	};
	
	//===============================
	var checkedID = window.localStorage.getItem('checkedID');
	function getDate (){
		return Date.now();
	}
	kai.prototype.getID = function() {
		if(checkedID === undefined || checkedID === null) {
			checkedID = getDate();
			window.localStorage.setItem('checkedID', checkedID);
		}
		return checkedID;
	}
	
	module.exports=kai;
});