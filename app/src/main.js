//globals define
define(function(require, exports, module) {
    'use strict';
	// import dependencies
var Engine = require('famous/core/Engine');
var Surface = require('famous/core/Surface');
var FlexibleLayout = require('famous/views/FlexibleLayout');
var ImageSurface = require('famous/surfaces/ImageSurface');
var Modifier = require('famous/core/Modifier');
var Transitionable = require('famous/transitions/Transitionable');
var kai = require('kai');
var kai1;

var json;
var taste = localStorage.getItem('cali');
console.log('this is cook: ' + taste);

var footer = '<i>Mobile Services Lab</i>, KTH';
var wsUri = 'ws://droid1.duckdns.org:8081';
var send = false; //flag for send/unsend;
var websocket;

var opacityTransform = new Transitionable(1);
var opacityTransform1 = new Transitionable(1);
var mainContext = Engine.createContext();

var ratios = [1, 2];
var layout = new FlexibleLayout({
  direction: 1,
  ratios: ratios
});
document.body.style.webkitTouchCallout='none';
var surfaces = [];

var surface1 = new Surface({
	size: [undefined, undefined],
	properties: {
		backgroundColor: 'white',
		backgroundImage: 'url(./img/top.png)',
		backgroundRepeat: 'no-repeat',
		backgroundSize: 'contain',
		backgroundPosition: 'center'
	}
	});
var surface2 = new Surface({
	size: [undefined, undefined],
	content : footer,
	properties: {
		backgroundImage: 'url(./img/plaid.jpg)',
		backgroundRepeat: 'no-repeat',
		backgroundSize: 'cover',
		backgroundPosition: 'center',
		color: 'white',
		textAlign: 'center',
		lineHeight: ((window.innerHeight*1.30933))+'px'
	}
	});

var image = new ImageSurface({
        size: [window.innerHeight * (881/1920) , window.innerHeight * (356/1920)]
    });

/*
var image1 = new ImageSurface({
        size: [window.outerWidth * (881/1920), window.outerHeight * (356/1920)]
    }); */

image.setContent('./img/button.png');
surfaces.push(surface1);
surfaces.push(surface2);

var originModifier = new Modifier({
  origin: [0.5, 0.5],
  align: [0.5, 0.580208333]
});

var originModifier1 = new Modifier({
  origin: [0.5, 0.5],
  align: [0.5, 0.780208333]
});

originModifier.opacityFrom(opacityTransform);
originModifier1.opacityFrom(opacityTransform1);

layout.sequenceFrom(surfaces);
mainContext.add(layout);
mainContext.add(originModifier).add(image);
//mainContext.add(originModifier1).add(image1);
mainContext.setPerspective(1);

mainContext.on('resize', function() {
	var size = mainContext.getSize();
	if (layout){
		layout.setOptions({size: [size[0], size[1]]});
		surface2.setOptions({properties: {lineHeight: (size[1]*1.30933)+'px'}});
		image.setOptions({size: [window.innerHeight * (881/1920) , window.innerHeight * (356/1920)]});
		//image1.setOptions({size: [window.innerHeight * (881/1920) , window.innerHeight * (356/1920)]});
	}
}.bind(this));

function onOpen(evt) {
	
	doSend("client " + kai1.getID());
	
	send = true;
	json = kai1.getJson();
}
function onClose(evt) {
	//writeToScreen("DISCONNECTED");
}
function onMessage(evt) {
    var fb = evt.data.split(" ")[0];
    console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!oops ' + evt.data);
    if(fb=='full') {
    	window.location = '/wait.html';
    	websocket.close();//!!!
    	return;
    }
    
	navigator.vibrate = navigator.vibrate || navigator.webkitVibrate || navigator.mozVibrate || navigator.msVibrate;
	if (navigator.vibrate) {
		navigator.vibrate(100);
	}

	var url = kai1.getUrl4garment(evt);
	console.log('!!!!!!!!!!!!!!!oops ' + url);
	window.location = url; //!!!
	
}
function onError(evt) {
	//writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
}
function doSend(message) {
	//writeToScreen("SENT: " + message);
	websocket.send(message);
}

function testWebSocket() {
	console.log('testWebSocket');
	websocket = new WebSocket(wsUri);
	websocket.onopen = function(evt) {
		onOpen(evt);
		console.log('onOpen');
	};
	websocket.onclose = function(evt) {
		onClose(evt);
		console.log('onclose');
	};
	websocket.onmessage = function(evt) {
		onMessage(evt);
	};
	websocket.onerror = function(evt) {
		onError(evt);
	};
}

var updateOrientation = function(e) {
	if (send && checkOnlyOne)
		doSend(kai1.updateMotion(e));//!!!
};

var checkOnlyOne = false; //
var updateCompass = function(e) {
	//console.log(': '+ e.alpha);
	if(checkOnlyOne )
		return;
	//checkOnlyOne = true;
	kai1.calibrateTo(e);
}

image.on('touchstart', function() {
	opacityTransform.set(0.6, {duration: 0});
	//this.image.setContent( 'img/button_pressed.png' );

	window.addEventListener('devicemotion', updateOrientation, false);
	//window.addEventListener('deviceorientation', updateCompass, false);
	send = true;
	checkOnlyOne = true;
});

image.on('touchend', function() {
	opacityTransform.set(1, {duration: 0});
	//this.image.setContent( 'img/button.png' );

	send = false;
	doSend('click');
	window.removeEventListener('devicemotion', updateOrientation, false);
	checkOnlyOne = false;
});

image.on('deploy', function() {
	kai1 = new kai();
	kai1.readJSON();
	
	testWebSocket();
	init();

	window.addEventListener('deviceorientation', updateCompass, false);
});

//tricked static variable
function countMyself() {
    if ( typeof countMyself.counter == 'undefined' ) {
        countMyself.counter = 0;
    }
    console.log('***'+ ++countMyself.counter);
}

var caliFunction = function(e) {
	kai1.caliFunc(e);//!!!
};

function absorbEvent_(event) {
			var e = event || window.event;
			e.preventDefault && e.preventDefault();
			e.stopPropagation && e.stopPropagation();
			e.cancelBubble = true;
			e.returnValue = false;
			return false;
		}
function preventLongPressMenu(node) {
			node.ontouchstart = absorbEvent_;
			node.ontouchmove = absorbEvent_;
			node.ontouchend = absorbEvent_;
			node.ontouchcancel = absorbEvent_;
		}
function init() {
			var all = document.getElementsByTagName("*");
			for (var i=0, max=all.length; i < max; i++) {
				preventLongPressMenu(all[i]);
			}
		}

});
