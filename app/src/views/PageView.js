define(function(require, exports, module) {
    var View            = require('famous/core/View');
    var Surface         = require('famous/core/Surface');
    var Transform       = require('famous/core/Transform');
    var StateModifier   = require('famous/modifiers/StateModifier');
	var FlexibleLayout = require('famous/views/FlexibleLayout');
    var HeaderFooter    = require('famous/views/HeaderFooterLayout');
    var ImageSurface    = require('famous/surfaces/ImageSurface');
    var Transitionable = require('famous/transitions/Transitionable');
	var Modifier = require('famous/core/Modifier');
	var kai = require('kai');
	var CustomSocket = require('customSocket');
	var surfaces = [];
	var ratios = [1, 2];
	var footer = '<i>Mobile Services Lab</i>, KTH';
	var kai1;
	var wsUri = 'ws://130.237.212.221:50002/connect'; //'ws://kyu.duckdns.org:50002/connect' //'ws://130.229.180.105:50002/connect'

    function PageView() {
        View.apply(this, arguments);

        _createLayout.call(this);
        _createBody.call(this);

        _setListeners.call(this);
    }

    PageView.prototype = Object.create(View.prototype);
    PageView.prototype.constructor = PageView;

    PageView.DEFAULT_OPTIONS = {
        headerSize: 0
    };

    function _createLayout() {
        this.layout = new FlexibleLayout({
            direction: 1,
			ratios: ratios
        });

        var layoutModifier = new StateModifier({
            transform: Transform.translate(0, 0, 0.1)
        });

        this.add(this.layout);
    }


    var opacityTransform;
    function _createBody() {
	
		var originModifier = new Modifier({
			origin: [0.5, 0.5],
			align: [0.5, 0.580208333]
		});

		var originModifier1 = new Modifier({
		  origin: [0.5, 0.5],
		  align: [0.5, 0.780208333]
		});

		opacityTransform = new Transitionable(1);
		originModifier.opacityFrom(opacityTransform);
	
		this.image = new ImageSurface({
			size: [window.innerHeight * (881/1920) , window.innerHeight * (356/1920)],
			content : 'img/button.png'
		});
		this.image1 = new ImageSurface({
			size: [window.innerHeight * (881/1920), window.innerHeight * (356/1920)],
			content : 'img/search.png'
		});
		
		this.surface1 = new Surface({
			size: [undefined, undefined],
			properties: {
				backgroundColor: 'white',
				backgroundImage: 'url(./img/top.png)',
				backgroundRepeat: 'no-repeat',
				backgroundSize: 'contain',
				backgroundPosition: 'center',
			}
			});
		this.surface2 = new Surface({
			size: [undefined, undefined],
			//content : footer, //blocked for EuroCis
			properties: {
				backgroundImage: 'url(./img/plaid.jpg)',
				backgroundRepeat: 'no-repeat',
				backgroundSize: 'cover',
				backgroundPosition: 'center',
				color: 'white',
				textAlign: 'center',
				lineHeight: ((window.innerHeight*1.30933))+'px'
			}
			});
			
		surfaces.push(this.surface1);
		surfaces.push(this.surface2);
		
		this.layout.sequenceFrom(surfaces);
        this.add(originModifier).add(this.image);
		//this.add(originModifier1).add(this.image1); //for EuroCis
		
    }
	
	var updateOrientation = function(e) {
		//console.log('update');
		//var kai1 = new kai();
		//if (send) 
		socket.doSend(kai1.updateMotion(e));
	};

    function _setListeners() {
        //this.image1.on('touchstart', function() {
            //this._eventOutput.emit('menuToggle');
        //}.bind(this));
		kai1 = new kai();
		kai1.readJSON();
		socket = new CustomSocket(wsUri, kai1);
		
		this.image.on('touchstart', function() {
			//opacityTransform.set(0.6, {duration: 0}); //blocked for EuroCis
			this.image.setContent( 'img/button_pressed.png' );
			window.addEventListener('devicemotion', updateOrientation, false);
		}.bind(this));
		
		this.image.on('touchend', function() {
			//opacityTransform.set(1, {duration: 0}); //blocked for EuroCis
			this.image.setContent( 'img/button.png' );
			socket.doSend('click');
			window.removeEventListener('devicemotion', updateOrientation, false);

			kai1.calibrate();//!!!
		}.bind(this));

        this.image1.pipe(this._eventOutput);
		
		this.image.on('deploy', function(){ 
			init();
		}.bind(this));
    }
	
	function absorbEvent_(event) {
			var e = event || window.event;
			e.preventDefault && e.preventDefault();
			e.stopPropagation && e.stopPropagation();
			e.cancelBubble = true;
			e.returnValue = false;
			return false;
		}
		function preventLongPressMenu(node) {
			node.ontouchstart = absorbEvent_;
			node.ontouchmove = absorbEvent_;
			node.ontouchend = absorbEvent_;
			node.ontouchcancel = absorbEvent_;
		}
		function init() {
			var all = document.getElementsByTagName("*");
			for (var i=0, max=all.length; i < max; i++) {
				preventLongPressMenu(all[i]);
			}
		}

    module.exports = PageView;
});
